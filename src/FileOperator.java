package src;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wapn on 18.03.17.
 *
 * @author wapn
 * @implNote Klasa odpowiedzialna za obsluge pliku.
 * Posiada metody wczytania nazwy, czytania z pliku oraz
 * wyslania danych do klasy obliczajacej wyrazenie.
 */

public class FileOperator extends Menu {

    List<String> lines = new ArrayList<String>();
    private String fileName;

    @Override
    public void showMenu() {
        cleanScreen();
        showCoreMenu();
        System.out.println("Podaj dzialanie matematyczne i ");
        System.out.println("zatwierdz enterem.");
        System.out.println("Wpisz 0 aby wyjsc z trybu liczenia.");
        System.out.println("----------------------------------------------");

    }

    public void setFileName(String name) {
        this.fileName = name;
    }

    public void setStatement(String row) {
        System.out.println(row);

    }

    /**
     * @implNote Metoda czyta zawartosc pliku i wpisuje kolejne wiersze do
     * listy lists
     */
    public void readFile() {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader("File"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String line = null;
        try {
            line = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        while (line != null) {
            lines.add(line);
            try {
                line = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @implNote Wyslanie danych z listy lists do metody liczacej z klasy
     * Calculate
     */
    public void sendToCalculate() {
        Calculation calculate = new Calculation();
        for (String x : lines) {
            calculate.count(x);
            System.out.println();
        }
    }

}
