package src;

import java.util.ArrayList;

/**
 * Created by wapn on 18.03.17.
 *
 * @author wapn
 * @implNote Klasa obliczajaca dzialania na liczbach z tablic statycznych.
 * Glowna czesc calej aplikacji.
 */
public class Calculation {
    private final char DIGITS[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
    private final char MATH_SYMBOLS[] = {'+', '-', '*', '/'};
    ArrayList<Integer> Digits = new ArrayList<>();
    ArrayList<Character> Symbols = new ArrayList<>();
    int result;

    /**
     * @param statement - wyrazenie matematyczne w formie STRING
     * @return wynik dzialania na wyrazeniu
     * @implNote Tu sie dzieje duzo rzeczy na raz. Metoda antywzorcowa!
     * Potrzebny refaktoring.
     */
    public int count(String statement) {
        String number = "";
        int startNumber = 0;
        Digits.clear();
        Symbols.clear();

        for (int i = 0; i < statement.length(); i++) {
            if (ifCharArrayIsContain(statement.charAt(i), MATH_SYMBOLS)) {
                Symbols.add(statement.charAt(i));
                number = statement.substring(startNumber, (i));
                Digits.add(Integer.parseInt(number));
                number = "";
                startNumber = i + 1;
            }
        }
        //ostatni element
        number = statement.substring(startNumber, statement.length());
        Digits.add(Integer.parseInt(number));

        //   WYPISANIE LISTY LICZB I OPERATOROW
//        for (int x: Digits) {
//            System.out.println(x + ", ");
//        }
//        System.out.println("Lista symboli: ");
//        for (char x: Symbols) {
//            System.out.println(x + ", ");
//        }

        //WYPISANIE POSTACI ROWNANIA:
        int indexTablicyoperatorow = 0;
        result = Digits.get(0);
        for (int x = 1; x < Digits.size(); x++) {
            System.out.print(Digits.get(x - 1) + " " + Symbols.get(x - 1));


            switch (Symbols.get(x - 1)) {
                case '+':
                    result = result + Digits.get(x);
                    break;
                case '-':
                    result = result - Digits.get(x);
                    break;
                case '*':
                    result = result * Digits.get(x);
                    break;
                case '/':

                    result = result / Digits.get(x);
                    break;
            }
        }

        System.out.print(Digits.get(Digits.size() - 1) + " = " + result);
        return 0;
    }

    /**
     * @param element   znak ktory zostanie porownany z tablica
     * @param charArray tablica w ktorej szuka sie znaku
     * @return prawda - jesli znaleziono znak
     */
    private boolean ifCharArrayIsContain(char element, char charArray[]) {
        for (int i = 0; i < charArray.length; i++) {
            if (Character.compare(element, charArray[i]) == 0) {
                //System.out.println("Element: " + element);
                return true;
            }
        }
        return false;
    }

    //Przypadki:
    // 3--3,
    // 3+-3,
    //

    /**
     * @param operation wyrazenie ktore zostanie poddane analizie pod wzgledem poprawnosci skladni
     * @return TRUE jesli wyrazenie jest poprawne
     */
    public boolean isCorectSyntaxOfOperation(String operation) {
        for (int i = 0; i < operation.length(); i++) {
            if (ifCharArrayIsContain(operation.charAt(i), DIGITS)) {
                continue;
            } else if (i + 1 == operation.length()) {
                return false;
            } else if (i == 0) {
                if (operation.charAt(0) == '-') {
                    continue;
                } else {
                    return false;
                }
            } else if (ifCharArrayIsContain(operation.charAt(i), MATH_SYMBOLS)) {
                if (ifCharArrayIsContain(operation.charAt(i + 1), MATH_SYMBOLS)) {
                    return false;
                }
                continue;
            } else {
                return false;
            }
        }
        return true;
    }

}
