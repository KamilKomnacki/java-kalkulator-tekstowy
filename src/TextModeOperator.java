package src;

/**
 * Created by wapn on 18.03.17.
 *
 * @author wapn
 * @implNote Klasa odpowiedzialna za wprowadzanie danych przez konsole
 * uzytkownika.
 */
public class TextModeOperator extends Menu {
    private String statement;
    private int result;

    @Override
    public void showMenu() {
        cleanScreen();
        showCoreMenu();
        System.out.println("Podaj nazwe pliku wraz ze sciezka,");
        System.out.println("gdzie zapisane są dzialania matematyczne");
        System.out.println("ktore chcesz obliczyc: ");
    }

    /**
     * @param row pojedynczy wiersz
     * @return TRUE jezeli wyrazenie jest poprawne
     * @implNote Nastepuje wpisanie do pola w klasie, jesli wyrazenie jest
     * poprawne
     */
    public boolean setStatement(String row) {
        Calculation calculation = new Calculation();

        if (calculation.isCorectSyntaxOfOperation(row)) {
            statement = row;
            return true;
        } else {
            System.out.println("Wyrazenie ma niepoprawna skalnie!");
            System.out.println("Wprowadz jeszcze raz: ");
            return false;
        }
    }

    public void count() {
        Calculation calculation = new Calculation();
        result = calculation.count(statement);
    }


    public int getResult() {
        return result;
    }
}


