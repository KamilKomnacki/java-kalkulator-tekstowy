package src;

/**
 * Created by wapn on 18.03.17.
 *
 * @author wapn
 * @implNote Klasa sluzaca do wyswietlania menu tekstowych.
 * TA KLASA JEST ABSRAKCYJNA
 */
public abstract class Menu {

    /**
     * @implNote Wyswietla naglowek w oknie konsoli
     */
    public static void showCoreMenu() {
        System.out.println("----------------------------------------------");
        System.out.println("Kalkulator tekstowy. KAMIL KOMNACKI WMP - UKSW");
        System.out.println("----------------------------------------------");
    }

    /**
     * @implNote Metoda do czyszczenia zawartosci konsoli
     */
    public static void cleanScreen() {
        for (int i = 0; i < 100; i++) {
            System.out.println();
        }
    }

    public abstract void showMenu();
}
