package src;

import java.util.Scanner;

/**
 * Created by wapn on 18.03.17.
 *
 * @author wapn
 * @implNote Klasa reprezentuje petle glowna w programie
 * dziedziczy po klasie abstrakcyjnej MENU
 */
public class MainLoop extends Menu {


    @Override
    public void showMenu() {
        showCoreMenu();
        System.out.println("1. Oblicz z lini komend.");
        System.out.println("2. Oblicz z pliku.");
        System.out.println("3. O programie.");
        System.out.println("0. Wyjscie.");
        System.out.println("----------------------------------------------");
    }

    public void main() {
        Scanner scanner = new Scanner(System.in);
        String choose = "";


        do {
            cleanScreen();
            showMenu();
            choose = scanner.nextLine();
            switch (choose.charAt(0)) {
                case '1':
                    TextModeOperator textMode = new TextModeOperator();
                    textMode.showMenu();
                    textMode.setStatement(scanner.nextLine());

                    textMode.count();
                    break;

                case '2':
                    FileOperator fileMode = new FileOperator();
                    fileMode.setFileName("File.txt");
                    fileMode.showMenu();
                    fileMode.readFile();
                    fileMode.sendToCalculate();
                    break;

                case '3':
                    System.out.println("Aplikacja pisana po nocach.");
                    System.out.println("Zawiera mnostwo bledow, ale teraz wiem jak wazna jest sytematyczna praca...");
                    break;

                default:
                    break;

            }
            System.out.println("Wybor: " + choose.charAt(0));
            scanner.nextLine();
        } while (Character.compare(choose.charAt(0), '0') != 0);
        System.out.println("Wybor: " + choose.charAt(0));
    }


}

